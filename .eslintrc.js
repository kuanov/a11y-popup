module.exports = {
  parser: "babel-eslint",
  extends: ["airbnb-base", "prettier"],
  env: {
    browser: true,
  }
};
