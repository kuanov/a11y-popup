import FOCUSABLE_ELEMENTS from "./helpers/focusableElements";

const ESCAPE_KEY = 27;
const TAB_KEY = 9;
const OPEN_ATTR = "open";
const POPUP_SELECTOR = "details.dialog";
const POPUP_BODY_SELECTOR = "[role=document]";

const getFocusableChilds = (el) =>
  Array.from(el.querySelectorAll(FOCUSABLE_ELEMENTS.join()));

const popupList = Array.from(document.querySelectorAll(POPUP_SELECTOR));
const popupBodyList = popupList.map((popup) =>
  popup.querySelector(POPUP_BODY_SELECTOR)
);
const popupBodyListFocusableEls = popupBodyList.map((popupBody) =>
  getFocusableChilds(popupBody)
);

popupList.forEach((popup, idx) =>
  popup.addEventListener("toggle", (e) => {
    if (
      e.target.hasAttribute(OPEN_ATTR) &&
      popupBodyListFocusableEls[idx].length
    ) {
      popupBodyListFocusableEls[idx][0].focus();
    }
  })
);

const isExtremumIdx = (up, arr, el) =>
  up ? arr.indexOf(el) === 0 : arr.indexOf(el) === arr.length - 1;

const setFocusToNextEl = (up, arr) =>
  up ? arr[arr.length - 1].focus() : arr[0].focus();

popupBodyList.forEach((popupBody, idx) => {
  popupBody.addEventListener("keydown", (e) => {
    if (!popupList[idx].hasAttribute(OPEN_ATTR)) return;

    if (e.keyCode === ESCAPE_KEY) {
      popupList[idx].removeAttribute(OPEN_ATTR);
    }

    if (
      e.keyCode === TAB_KEY &&
      isExtremumIdx(
        e.shiftKey,
        popupBodyListFocusableEls[idx],
        document.activeElement
      )
    ) {
      e.preventDefault();
      setFocusToNextEl(e.shiftKey, popupBodyListFocusableEls[idx]);
    }
  });
});

// в случай если курсор находится в консоли dev-tools и нажимаем таб
const getOpenPopupFocusableEls = () =>
  popupBodyListFocusableEls.find(
    (_, i) => i === popupList.findIndex((p) => p.hasAttribute(OPEN_ATTR))
  );

document.addEventListener("focusin", (e) => {
  if (
    popupList.some((popup) => popup.hasAttribute(OPEN_ATTR)) &&
    getOpenPopupFocusableEls().length &&
    !getOpenPopupFocusableEls().includes(e.target)
  ) {
    getOpenPopupFocusableEls()[0].focus();
  }
});
